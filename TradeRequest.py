class TradeRequest:
    stock: Stock
    amount: int

    def buy(stock: Stock, amount: int, user: User) -> None:
        price = stock.getPrice() * amount
        if not user.personalCurrency.validFunds(price):
            throwError()
            return
        user.personalCurrency.subtract(price)
        user.personalCurrency.updateServerside(user.personalCurrency.amount)
        for i in range(amount):
            user.stockList.add(stock)
        

    def sell(stock: Stock, amount: int, user: User) -> None:
        price = stock.getPrice() * amount
        user.personalCurrency.add(price)
        user.personalCurrency.updateServerside(user.personalCurrency.amount)
        for i in range(amount):
            user.stockList.remove(stock)
        
    
    def TradeRequest(stock: Stock, amount: int):
        stock = stock
        amount = amount
