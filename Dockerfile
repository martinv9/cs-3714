# This file is a template, and might need editing before it works on your project.
FROM python:3.6

# Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        postgresql-client \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

COPY . /usr/src/app

# For Django
EXPOSE 8000
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]

# For some other command
# CMD ["python", "app.py"]

class Currency:
    Double amount
    Server server

    def getFunds() -> void:
        updateServerside(amount)
    
    def add(double add) -> bool:
        amount += add
        return True

    def subtract(double sub) -> bool:
        amount -= sub
        return True

    def validFunds(double amt) -> bool:
        if(amount >= amt)
            return True
        return False
    
    def updateServerside(double amount) -> bool:
        server.updateAmount(amount)
        return True