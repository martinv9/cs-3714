class User:
    String userName
    String password
    Currency personalCurrency
    List<TradeRequest> tradeHistory
    int personalRank
    bool dailyLogin
    List<Stock> stockList

    def checkLogin(userID) -> bool:
        dailyLogin = db.alreadyLoggedIn(userID)
        return dailyLogin
    
    def addLoginBonus(userID) -> None:
        if not (checkLogin(userID)):
            db.addToMoney(user, 10)
        None

    def getHistory(userID) -> List<TradeRequest>:
        return db.tradeHistory(userID)
        None

    def getRank(userID) -> int:
        return db.sortByRank().userID

    def setPassword(newpass) -> None:
        db.setpassword(user, newpass)
        None
